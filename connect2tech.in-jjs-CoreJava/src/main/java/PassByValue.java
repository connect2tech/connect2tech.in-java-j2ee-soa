public class PassByValue {
	
	static int v;

	public static void function(int x, int y) {
		v = x + 10;
		x = x + 10;
		y = y + 20;
		
		System.out.println("x="+x);
		System.out.println("y="+y);
		
	}

	public static void main(String[] args) {

		int a = 10;
		int b = 20;
		
		System.out.println("a="+a);
		System.out.println("b="+b);
		
		//function(a,b);
		
		System.out.println("a="+a);
		System.out.println("b="+b);
		
		System.out.println(v);

	}

}
