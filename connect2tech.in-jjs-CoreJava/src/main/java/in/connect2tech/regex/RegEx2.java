package in.connect2tech.regex;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegEx2 {
	public static void main(String[] args) {
		matchAlphabetsBackslash();
	}

	public static void matchAlphabetsBackslash() {
		List<String> names = new ArrayList<String>();

		names.add("L:okesh\\");
		names.add("D:\\nareshchaurasia\\JavaArchitect\\connecttechincorejava");
		names.add("LOkesh123-"); // Incorrect

		String regex = "^[a-zA-Z0-9/\\\\:]+$";

		Pattern pattern = Pattern.compile(regex);

		for (String name : names) {
			Matcher matcher = pattern.matcher(name);
			System.out.println(matcher.matches());
		}
	}
}
