package in.connect2tech.interview.misc;

public class Varargs {
	static void display(String... values) {
		System.out.println("display method invoked "+values.length);
		
		for (int i = 0; i < values.length; i++) {
			String string = values[i];
			System.out.println(string);
		}
		
	}

	public static void main(String args[]) {

		display();// zero argument
		display("my", "name", "is", "varargs");// four arguments
	}
}
