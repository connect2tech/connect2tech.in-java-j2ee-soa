package in.connect2tech.interview.generics;

import java.util.*;

class Animal1 {
	public void eat() {

	}
}

class Cat1 extends Animal1 {

}

class Dog1 extends Animal1 {

}

public class Arrays {
	public static void main(String[] args) {
		Animal1 Animal1[] = { new Dog1() };
		sortingArray(Animal1);

		/*List<Dog1> Dog1s = new ArrayList<Dog1>();
		Dog1s.add(new Dog1());
		sortingList(Dog1s);*/

	}

	private static void sortingArray(Animal1 Animal1[]) {
		Animal1[0] = new Cat1();
		System.out.println(Animal1[0]);
	}

	private static void sortingList(List<? extends Animal1> Animal1s) {
		//Animal1s.add(new Dog1());
		System.out.println(Animal1s.get(0));
	}
}