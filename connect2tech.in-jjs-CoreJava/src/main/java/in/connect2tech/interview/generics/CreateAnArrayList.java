package in.connect2tech.interview.generics;

import java.util.*;

class GenericMethod {
	public <T> void makeArrayList(T t) { // take an object of an
		// unknown type and use a
		// "T" to represent the type
		List<T> list = new ArrayList<T>(); // now we can create the
		// list using "T"
		list.add(t);
	}
}

public class CreateAnArrayList {

}