package in.connect2tech.interview.java8.lambda;

interface Draw {
	void draw();
}

public class Lambda1 {
	public static void main(String[] args) {
		Draw d = new Draw() {
			public void draw() {
				System.out.println("Draw without lambda..");
			}
		};

		d.draw();

		Draw d2 = () -> {
			System.out.println("Draw with lambda..");
		};

		d2.draw();
		
		Draw d3 = () -> System.out.println("Draw with lambda..");

		d3.draw();

	}

}
