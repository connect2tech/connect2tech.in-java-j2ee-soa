package in.connect2tech.interview.java8.lambda;

interface Addition {
	int add(int a, int b);
}

public class Lambda3 {
	public static void main(String[] args) {

		Addition a = (x,y)->{
			return x + y;
		};
		
		System.out.println(a.add(10, 20));
	}

}
