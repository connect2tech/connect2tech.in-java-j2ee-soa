package in.connect2tech.interview.java8.lambda;

public class Lambda11 {

	public static void main(String[] args) {
		double value = 4.7;
		LambdaType1 circleArea = val -> System.out.println(Math.PI * val * val);
		LambdaType1 squareArea = val -> System.out.println(val * val);
		circleArea.xyz_nameDoesNotMatter(value);
		squareArea.xyz_nameDoesNotMatter(value);
	}

}

interface LambdaType1 {
	void xyz_nameDoesNotMatter(double val);
}