package in.connect2tech.java8;

interface Dance {
	void dance(int x);
}

public class Lambda1 {
	public static void main(String[] args) {
		Dance d;

		d = new Dance() {
			public void dance(int x) {
				System.out.println("Salsa1");
			}
		};

		d = x -> System.out.println("Salsa1");

		d = x -> System.out.println("Salsa2"); // Lambdas, consumers

		d.dance(10);
	}
}
