package in.connect2tech.java5.comparisons;

import java.util.Comparator;

public class AgeComparatorReverse<T> implements Comparator<T> {

	Comparator<T> delegateComparator;

	AgeComparatorReverse(Comparator<T> delegateComparator) {
		this.delegateComparator = delegateComparator;
	}

	@Override
	public int compare(T left, T right) {
		return -1 * delegateComparator.compare(left, right);
	}

}
