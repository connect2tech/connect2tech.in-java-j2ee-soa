package in.connect2tech.java5.generic;

public class UsingStack {
	public static void main(String args[]) {
		Stack<String> s = new Stack<String>();
		System.out.println(s.pop() + " Popped from stack");
	}
}