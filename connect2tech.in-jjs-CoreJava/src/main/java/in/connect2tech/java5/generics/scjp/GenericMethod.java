package in.connect2tech.java5.generics.scjp;

import java.util.ArrayList;
import java.util.List;

public class GenericMethod {

}

class CreateAnArrayList {
	public <T> void makeArrayList(T t) {
		List<T> list = new ArrayList<T>();
		list.add(t);
	}
}
