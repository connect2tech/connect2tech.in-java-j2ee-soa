package in.connect2tech.java5.generics.scjp;

public class ArrayStoreExceptionExample {
	public static void main(String args[]) {
		
		// Since Double class extends Number class
		// only Double type numbers
		// can be stored in this array
		Number[] a = new Double[2];
		// Trying to store an integer value
		// in this Double type array. It will not work
		a[0] = new Integer(4);
		
		//This works as you have created place to store numbers.
		Number[] b = new Number[2];
		b[0] = new Integer(10);
		
		System.out.println("If it mixed array, then it is allowed!!!");
	}
}
