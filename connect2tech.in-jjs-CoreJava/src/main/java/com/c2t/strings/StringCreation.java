package com.c2t.strings;

public class StringCreation {
	public static void main(String[] args) {
		String s1  = "Thenu";
		
		String s2 = new String("Shobhit");
		
		System.out.println(s1);
		System.out.println(s2);
		
		String s3 = s1 + s2;
		
		System.out.println(s3);
		
		String s4 = s2 + 10;
		System.out.println(s4);
		
	}
	
}
