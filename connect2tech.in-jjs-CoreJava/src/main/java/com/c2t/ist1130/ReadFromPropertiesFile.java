package com.c2t.ist1130;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ReadFromPropertiesFile {
	public static void main(String[] args) throws Exception{
		File f = new File(
				"D:/nchaurasia/Java-Architect/connect2tech.in-CoreJava/src/main/java/com/c2t/ist1130/selenium.properties");
		InputStream is = new FileInputStream(f);
		
		Properties prop = new Properties();
		prop.load(is);
		System.out.println(prop);
		System.out.println(prop.get("name"));
	
	}
}
