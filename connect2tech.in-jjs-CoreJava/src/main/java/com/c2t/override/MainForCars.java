package com.c2t.override;

import java.util.Scanner;

public class MainForCars {
	public static void main(String[] args) {
		/*
		 * Car car = new Car(); car.startEngine();
		 * 
		 * Hyundai hyundai = new Hyundai(); hyundai.startEngine();
		 * 
		 * Volvo volvo = new Volvo(); volvo.startEngine();
		 */
		
		System.out.println("Enter the value of a...");
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();
		


		Car c1 = null;

		if ( a > 10) {
			c1 = new Volvo();
		} else {
			c1 = new Hyundai();
		}

		c1.startEngine();
	}
}
