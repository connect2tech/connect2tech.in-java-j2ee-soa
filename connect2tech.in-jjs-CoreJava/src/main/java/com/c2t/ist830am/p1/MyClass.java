package com.c2t.ist830am.p1;

import com.c2t.ist830am.p2.File2;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class File1 extends File2{
	void display1(){
		System.out.println("I am file1...");
		
		File1 f = new File1();
		f.display_protected();
		
	}
}

public class MyClass {
	public static void main(String[] args) {
		File1 f1 = new File1();
		f1.display1();
		
		File2 f2 = new File2();
		f2.display2();
		//f2.display_default();
	}
}
