package com.c2t.ist830am.p2;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class File2 {
	
	protected void display_protected(){
		System.out.println("display_protected...");
	}
	
	private static void display_pvt(){
		System.out.println("display_pvt...");
	}
	
	static void display_default(){
		System.out.println("display_pvt...");
	}
	
	public void display2(){
		System.out.println("display_default...");
		
		display_pvt();

	}
}

class File3{
	public void method(){
		File2 f = new File2();
		f.display_default();
	}
}
