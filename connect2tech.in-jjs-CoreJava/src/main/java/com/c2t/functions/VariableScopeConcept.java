package com.c2t.functions;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class VariableScopeConcept {
	
	static int a = 10;
	
	public static void main(String[] args) {
		int b = 20;
		int a = 100;
		System.out.println("a="+a);
		System.out.println("b="+b);
	}
	
	public static void method(){
		System.out.println("a="+a);
		//System.out.println("b="+b);
		
	}

}
