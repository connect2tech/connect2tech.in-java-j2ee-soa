package com.c2t.interfaces;


//concrete
abstract class Shape{
	abstract void draw();
	
	void color(){
		System.out.println("I am inside color...");
	}
}

class Circle extends Shape{
	void draw(){
		System.out.println("I am inside draw Circle...");
	}
}

class Rectangle extends Shape{
	void draw(){
		System.out.println("I am inside draw Rectangle...");
	}
}


public class AbstractClassExample {
	public static void main(String[] args) {
		Shape s = new Circle();
		s.draw();
	}
}
