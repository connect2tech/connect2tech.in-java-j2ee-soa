package com.c2t.threads.deadlock;

class Thread1 extends Thread {

	String apple;
	String orange;

	Thread1(String s1, String s2) {
		apple = s1;
		orange = s2;
	}

	int i;

	public void run() {
		while (true) {
			System.out.println("Thread1.......");
			synchronized (orange) {
				synchronized (apple) {
					System.out.println("Thread id2..." + Thread.currentThread().getName());

					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}

		}
	}
}

class Thread2 extends Thread {
	String apple;
	String orange;

	Thread2(String s1, String s2) {
		apple = s1;
		orange = s2;
	}

	int j;

	public void run() {

		while (true) {
			
			synchronized (apple) {
				
				synchronized (orange) {
					System.out.println("T1="+Thread.currentThread().getName());
				}
				
				try {
					sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		}

	}
}

public class DeadLocking {

	public static void main(String[] args) {

		String apple = "apple";
		String orange = "orange";

		Thread1 anbu = new Thread1(apple, orange);
		Thread2 prasuna = new Thread2(apple, orange);

		anbu.start();
		prasuna.start();
	}

}
