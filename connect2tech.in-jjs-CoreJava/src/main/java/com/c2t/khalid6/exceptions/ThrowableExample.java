package com.c2t.khalid6.exceptions;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ThrowableExample {
	public static void main(String[] args) {
		Throwable t = new Throwable("Custom Message");
		System.out.println(t.getMessage());
		t.printStackTrace();
	}
}
