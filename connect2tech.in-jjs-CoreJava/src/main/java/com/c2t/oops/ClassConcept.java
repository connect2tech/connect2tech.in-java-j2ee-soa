package com.c2t.oops;

class Person{
	int age;
	String name;
	
	void eat(){
		System.out.println("i can eat...");
	}
}

public class ClassConcept {
	public static void main(String[] args) {
		
		int a =  10;
		
		Person p = new Person();
		p.name =  "java";
		p.age = 10;
		
		System.out.println(p.name);
		System.out.println(p.age);
		
		p.eat();
		
		Person p2 = new Person();
		p2.name = "selenium";
		p2.age = 15;
		
		System.out.println(p2.name);
		System.out.println(p2.age);
		
		
		 
	}
}
