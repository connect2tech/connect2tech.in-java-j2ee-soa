package com.c2t.oops.edureka;

class Employee{
	String name;
	int age;
	double d;
	
	public void display(){
		System.out.println("name="+name);
		System.out.println("age="+age);
		System.out.println("d="+d);
	}
}


public class DefaultValues {
	public static void main(String[] args) {
		Employee e = new Employee();
		e.display();
	}
}
