package com.c2t.oops;

class StudentEdureka {

	String name;
	static int count = 0;
	
	static{
		//name = "hello";
		count = 100;
	}

	StudentEdureka() {
		++count;
	}

	static void staticMethod() {
		System.out.println("I am static method...");
	}

}

public class Mains {
	public static void main(String[] args) {

		//StudentEdureka.staticMethod();

	}
}
