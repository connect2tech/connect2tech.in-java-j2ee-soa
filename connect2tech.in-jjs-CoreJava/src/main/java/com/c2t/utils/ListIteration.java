package com.c2t.utils;

import java.util.*;

public class ListIteration {
	public static void main(String[] args) {
		List<String> topics = new ArrayList<String>();

		// add 4 different values to list
		topics.add("Java");
		topics.add("J2ee");
		topics.add("Selenium");
		topics.add("Python");
		
		//Basic for loop
		/*for(int i=0;	i<topics.size();	i++){
			System.out.println(topics.get(i));
		}*/
		
		//Advanced for loop
		/*for(String str:topics){
			System.out.println(str);
		}*/
		

		
		System.out.println(topics);
		Iterator <String> iter = topics.iterator();
		
		while(iter.hasNext()){
			String str = iter.next();
			
			if(str.equals("Selenium")){
				iter.remove();
			}
			
			//System.out.println(str);
		}
		
		System.out.println(topics);
		

		/*Enumeration<String> en = Collections.enumeration(topics);
		
		while(en.hasMoreElements()){
			String s = en.nextElement();
			System.out.println(s);
		}*/
		
	}
}
