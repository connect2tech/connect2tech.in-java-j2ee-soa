package com.c2t.utils;

import java.util.*;



public class MyFirstHashMap2 {
	public static void main(String[] args) {
		Map <String, String> m = new HashMap<String, String>();
		
		m.put("k1", "v1");
		m.put("k2", "v2");
		
		Set <String> s = m.keySet();
		System.out.println(s);
		
		Iterator <String> iter = s.iterator();
		
		while(iter.hasNext()){
			String key = iter.next();
			System.out.println("key="+key);
			System.out.println("val="+m.get(key));
		}
		
		
		
	}
}
