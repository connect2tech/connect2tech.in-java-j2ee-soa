package com.c2t.exception;

public class TryCatchFinally {

	public static void main(String[] args) {

		int arr[] = { 10, 20, 30 };

		int a = 10;
		int b = 0;

		System.out.println("Before division");

		try {
			int c = a / b;
			System.out.println(c);
			System.out.println(arr[3]);

			System.out.println("i am in try block");
			
		} 
		finally{
			System.out.println("i am inside finally...");
		}
 
		System.out.println("After divison");

	}

}
