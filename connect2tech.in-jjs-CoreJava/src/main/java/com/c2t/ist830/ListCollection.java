package com.c2t.ist830;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ListCollection {
	public static void main(String[] args) {
		ArrayList <String> al = new ArrayList<String>();
		
		al.add("one");
		al.add("two");
		al.add("three");
		al.add("one");
		
		System.out.println(al);
		
		//add(int index, E element)
		//get(int index)
		//indexOf(Object o)
		
		for(int i=0;i<al.size();i++){
			System.out.println(al.get(i));
		}
		
		Iterator<String> iter = al.iterator();
		
		while(iter.hasNext()){
			String val = iter.next();
			System.out.println(val);
		}
		
	}
}
