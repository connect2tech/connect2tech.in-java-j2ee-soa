
class Student {
	
	static{
		System.out.println("I am static block...");
	}

	Student() {
		System.out.println("I am constructor...");
	}
}

public class StaticConcepts {
	public static void main(String[] args) {
		Student s = new Student();
	}
}
