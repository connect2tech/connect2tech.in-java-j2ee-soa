Head First Object Oriented Analysis and Design
==============================================


## The second largest heading

###### The smallest heading

**This is bold text**

*This text is italicized*

~~This was mistaken text~~

**This text is _extremely_ important**

In the words of Abraham Lincoln:

> Pardon my French



1. James Madison
2. James Monroe
3. John Quincy Adams

- [x] Finish my changes
- [ ] Push my commits to GitHub
- [ ] Open a pull request

## Chapter 1

- This design is terrible! The  Inventory and Guitar classes  depend on each other too much, and I  can�t see how this is an architecture  that you�d ever be able to  build upon. We need some  restructuring. 

```
public Guitar search(Guitar searchGuitar) {
}
```

- In the better-designed areas of Objectville, objects  are very particular about their jobs. Each object is  interested in doing its job, and only its job, to the best  of its ability. There�s nothing a well-designed object  hates more than being used to do something that really  isn�t its true purpose. 

	- (1) Objects should do what their names indicate.  If an object is named Jet, it should probably takeOff()  and land(), but it shouldn�t takeTicket()�that�s the job  of another object, and doesn�t belong in Jet.  

	- (2) Each object should represent a single concept.  You don�t want objects serving double or triple duty.  Avoid a Duck object that represents a real quacking  duck, a yellow plastic duck, and someone dropping  their head down to avoid getting hit by a baseball.  

	- (3) Unused properties are a dead giveaway.  If you�ve got an object that is being used with no-value  or null properties often, you�ve probably got an object  doing more than one job. If you rarely have values for a  certain property, why is that property part of the object?  Would there be a better object to use with just a subset  of those properties? 
	
- Frank: Encapsulation is also about breaking your app into logical  parts, and then keeping those parts separate. So just like you keep the  data in your classes separate from the rest of your app�s behavior, we  can keep the generic properties of a guitar separate from the actual  Guitar object itself.

- If you get stuck, think about the things that are  common between the Guitar object and what a  client would supply to the search() method.
	- **Here serial number and amount is not relevant, so encapsulation happens.**

- Anytime you see  duplicate code, look for a  place to encapsulate!

- The idea behind encapsulation is to protect information  in one part of your application from the other parts of your  application. In its simplest form, you can protect the data  in your class from the rest of your app by making that data  private. But sometimes the information might be an entire  set of properties�like the details about a guitar�or even  behavior�like how a particular type of duck flies.  When you break that behavior out from a class, you can  change the behavior without the class having to change as  well. So if you changed how properties were stored, you  wouldn�t have to change your Guitar class at all, because  the properties are encapsulated away from Guitar.  That�s the power of encapsulation: by breaking up the different  parts of your app, you can change one part without having to  change all the other parts. In general, you should encapsulate  the parts of your app that might vary away from the parts that  will stay the same. 

## Chapter 2

- Requirements are things your  system must do to work correctly.  
- Your initial requirements usually  come from your customer.  
- To make sure you have a good  set of requirements, you should  develop use cases for your  system.  
- Use cases detail exactly what your  system should do.  
- A use case has a single goal, but  can have multiple paths to reach  that goal.  
- A good use case has a starting  and stopping condition, an  external initiator, and clear value  to the user.  
- A use case is simply a story about  how your system works.  
- You will have at least one use case  for each goal that your system  must accomplish.  
- After your use cases are complete,  you can refine and add to your  requirements.  
- A requirements list that makes all  your use cases possible is a good  set of requirements.  
- Your system must work in the real  world, not just when everything  goes as you expect it to.  
- When things go wrong, your  system must have alternate paths  to reach the system�s goals. 

## Chapter 3