# H1
## H2
### H3
#### H4
##### H5
###### H6

Alternatively, for H1 and H2, an underline-ish style:

Alt-H1
======

Alt-H2
------

Three or more...

---

Hyphens

***

Asterisks

___

Underscores

connect2tech.in-jjs-CoreJava
============================

# Generics

[Pluralsight](https://app.pluralsight.com/library/courses/java-generics/table-of-contents)

- Using Comparable
```
public class Student implements Comparable<Student>{
...
}
```

- Using Comparator
```
public class AgeComparator implements Comparator<Employee> {
...
}
```

- Reverse Comparator
```
public class ReverseComparator<T> implements Comparator<T> {
	Comparator<T> delegateComparator;
	ReverseComparator(Comparator<T> delegateComparator) {
		this.delegateComparator = delegateComparator;
	}
	@Override
	public int compare(T left, T right) {
		return -1 * delegateComparator.compare(left, right);
	}
}
```
___

**Type Bound**

```
public class SortedPairTypeBounds<T extends Comparable<T>> {
...
}
```
___

**Generic Method**

```
static <T> void genericDisplay(T element) {
...
}
```


___