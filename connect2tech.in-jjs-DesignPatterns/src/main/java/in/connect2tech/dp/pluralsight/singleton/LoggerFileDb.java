package in.connect2tech.dp.pluralsight.singleton;

public class LoggerFileDb {

	static LoggerFileDb logger = new LoggerFileDb();
	
	private LoggerFileDb() {
	}

	static LoggerFileDb getLogger() {
		return logger;
	}
	
	public static void doLogging(){
		
	}
	
}
