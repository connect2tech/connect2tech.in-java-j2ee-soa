
/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class SingleDimensionalArray {
	public static void main(String[] args) {
		
		int array [] = new int [5];
		
		int [] array2  = new int [5];
		
		int arr [] = {10,20,30,40,50};
		
		int arr2 [] = new int []{10,20,30,40,50};
		
		System.out.println(arr.length);
		
		for(int i=0;i<5;i++){
			array[i] = i + 10;
			
			System.out.println(array[i]);
		}
		
		
	}
}
