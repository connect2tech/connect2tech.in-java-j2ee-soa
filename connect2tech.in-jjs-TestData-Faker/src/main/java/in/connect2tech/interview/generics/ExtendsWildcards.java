package in.connect2tech.interview.generics;

import java.util.*;

class WildCards {
	/**
	 * I promise not to add anything to list
	 * 
	 * @param list
	 */
	void methodExtends(List<? extends Number> list) {
		System.out.println(list);
	}

	void methodSuper(List<? super Number> list) {
		list.add(25);
		System.out.println(list);
	}

	/**
	 * This will accept any type of list, but no addition to the list is
	 * allowed.
	 * 
	 * @param list
	 */
	void onlyWild(List<?> list) {
		// list.add(25);
		System.out.println(list);
	}

}

public class ExtendsWildcards {
	public static void main(String[] args) {

		WildCards wildCards = new WildCards();

		List<Integer> myList = new ArrayList<Integer>();
		myList.add(10);
		myList.add(20);

		wildCards.methodExtends(myList);

		List<Object> myList2 = new ArrayList<Object>();
		myList2.add(10);
		wildCards.methodSuper(myList2);

	}
}
