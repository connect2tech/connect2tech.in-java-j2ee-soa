package in.connect2tech.interview.exceptions;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class StorageExceptions {
	public static void main(String args[]) {

		// Since Double class extends Number class
		// only Double type numbers
		// can be stored in this array
		Number[] a = new Double[2];

		// Trying to store an integer value
		// in this Double type array
		a[0] = new Integer(4);
	}
}
