package in.connect2tech.java5.generic_non;

public class Stack {
	static final int MAX = 5;
	int top;
	Object a[] = new Object[MAX]; // Maximum size of Stack

	boolean isEmpty() {
		return (top < 0);
	}

	Stack() {
		top = -1;
	}

	boolean push(Object x) {
		if (top >= (MAX - 1)) {
			System.out.println("Stack Overflow");
			return false;
		} else {
			a[++top] = x;
			System.out.println(x + " pushed into stack");
			return true;
		}
	}

	Object pop() {
		if (top < 0) {
			System.out.println("Stack Underflow");
			return 0;
		} else {
			Object x = a[top--];
			return x;
		}
	}

	Object peek() {
		if (top < 0) {
			System.out.println("Stack Underflow");
			return 0;
		} else {
			Object x = a[top];
			return x;
		}
	}
}