package in.connect2tech.java5.comparisons;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

import java.util.Comparator;

public class Student1Comparator implements Comparator<Student1> {

	@Override
	public int compare(Student1 o1, Student1 o2) {
		if (o1.getId() >= o2.getId()) {
			return 1;
		} else if (o1.getId() < o2.getId()) {
			return -1;
		}
		return 0;
	}

}