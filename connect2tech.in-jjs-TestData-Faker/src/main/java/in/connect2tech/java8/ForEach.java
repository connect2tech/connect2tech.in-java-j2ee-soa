package in.connect2tech.java8;

import java.util.ArrayList;
import java.util.List;

public class ForEach {
	public static void main(String[] args) {
		List<String> l = new ArrayList<String>();

		l.add("A");
		l.add("B");
		l.add("C");
		l.add("D");

		l.forEach(t -> System.out.println(t));
	}
}
