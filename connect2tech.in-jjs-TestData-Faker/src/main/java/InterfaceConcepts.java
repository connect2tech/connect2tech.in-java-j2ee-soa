
/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

interface Remote100{
	final int a = 10;
	public void powerOnOff();
	abstract public void changeVolume();
}

class TvRemote100 implements Remote100{
	public void powerOnOff(){
		System.out.println("power on/off");
		System.out.println(a);
		
		//a = 100;
	}
	
	public void changeVolume(){
		System.out.println("change volume");
	}
}

public class InterfaceConcepts {
	public static void main(String[] args) {
		Remote100 r = new TvRemote100();
		r.powerOnOff();
	}
}
