package com.c2t.innerclasses;

class MyOuter{
	int a = 10;
	
	class MyInner{
		int b=20;
		
		public void innerMethod(){
			System.out.println("a="+a);
			System.out.println("b="+b);
		}
	}
}


public class InnerMain {
	public static void main(String[] args) {
		MyOuter outer = new MyOuter();
		
		MyOuter.MyInner inner = outer.new MyInner();
		inner.innerMethod();
	}
}
