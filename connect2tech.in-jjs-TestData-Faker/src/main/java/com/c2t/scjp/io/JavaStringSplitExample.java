package com.c2t.scjp.io;

public class JavaStringSplitExample {
	
	public static void main(String args[]){
		  
		 
		  /* String to split. */
		  String str = "one|two|three";
		  String[] temp;
		 
		  /* delimiter */
		  String delimiter = "|";
		  /* given string will be split by the argument delimiter provided. */
		  temp = str.split(delimiter);
		  /* print substrings */
		  for(int i =0; i < temp.length ; i++){
			  System.out.println(temp[i]);
		  }
	}

}
