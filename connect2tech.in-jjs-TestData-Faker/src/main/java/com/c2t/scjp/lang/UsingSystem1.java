package com.c2t.scjp.lang;

import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

public class UsingSystem1 {
	public static void main(String[] args) {
		Properties prop = System.getProperties();
		System.out.println(prop);
		
		Set keys = prop.keySet();
		Iterator iter = keys.iterator();
		
		while(iter.hasNext()){
			String key = (String)iter.next();
			System.out.print(key + " = ");
			System.out.println(prop.getProperty(key));
		}
	}
}
