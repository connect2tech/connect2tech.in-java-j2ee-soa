package com.c2t.threads.sync2.edureka;

class PrintTables {

	synchronized public static void printTables(int number) {

		// System.out.println("number=="+number);
		for (int i = 1; i <= 10; i++) {
			System.out.println((number + " * " + i + " = ") + (number * i));
			// System.out.println("Thread.currentThread().getName()==" +
			// Thread.currentThread().getName());

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}

class Thread1 extends Thread {

	PrintTables pt1;

	Thread1(PrintTables p1) {
		pt1 = p1;
	}

	public void run() {
		PrintTables.printTables(5);
	}
}

class Thread2 extends Thread {

	PrintTables pt2;

	Thread2(PrintTables p2) {
		pt2 = p2;
	}

	public void run() {
		pt2.printTables(10);
	}
}

public class SyncBlock2 {
	public static void main(String[] args) {

		PrintTables p2 = new PrintTables();

		PrintTables p = new PrintTables();
		// p.printTables(5);

		Thread1 t1 = new Thread1(p);
		Thread2 t2 = new Thread2(p);

		t1.start();
		t2.start();
	}
}