package com.c2t.inheritance;

class GrandParent{
	GrandParent(){
		System.out.println("I am grandParent...");
	}
}

class Parent extends GrandParent{
	Parent(){
		System.out.println("I am parent...");
	}
}

class Child extends Parent{
	Child(){
		System.out.println("I am child...");
	}
}

public class IneritanceConcept2 {
	public static void main(String[] args) {

		Child c = new Child();

		
	}
}
