package com.c2t.drivers;

import java.util.Scanner;

interface Browser{
	public void launch();
}

class FF implements Browser{
	public void launch(){
		System.out.println("FF");
	}
}

class Chrome implements Browser{
	public void launch(){
		System.out.println("Chrome");
	}
}



public class LaunchingBrowsers {
	
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		System.out.print("Enter your brower: ");
		String driverName = in.next();
		
		Browser d = null;
		
		if(driverName.equals("Chrome")){
			d = new Chrome();
		}else{
			d = new FF();
		}
		d.launch();
		
		
		
		/*System.out.println("name="+name);
		
		FF f = new FF();
		f.launch();*/
	}

}
