package com.c2t.functions;

public class CreatingFunction {

	// Take milk
	// Warm up milk
	// Add water
	// Add coffee
	// Add sugar

	public static void main(String[] args) {
		
		powerOnMachine();
		
		String coffe1 = makeCoffee();
		String coffe2 = makeCoffee();
	}
	
	public static void powerOnMachine(){
		System.out.println("Start machine...");
	}
	
	public static String makeCoffee(){
		
		// Take milk
		// Warm up milk
		// Add water
		// Add coffee
		// Add sugar
		return "coffee";
	}
	
	
}
