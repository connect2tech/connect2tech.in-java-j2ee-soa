package com.c2t.strings;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class StringIntern {
	public static void main(String[] args) {
		
		String s2 = "Rakesh";
		String s3 = "Rakesh".intern();
		String s4 = new String("Rakesh");
		String s5 = new String("Rakesh").intern();
		String s1 = "Rakesh";

		if (s1 == s2) {
			System.out.println("s1 and s2 are same"); // 1.
		}

		if (s1 == s3) {
			System.out.println("s1 and s3 are same"); // 2.
		}

		if (s1 == s4) {
			System.out.println("s1 and s4 are same"); // 3.
		}	

		if (s1 == s5) {
			System.out.println("s1 and s5 are same"); // 4.
		}
	}
}
