package com.c2t.strings;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class StringToByteArray {
	public static void main(String[] args) {
		String s = "AA";
		
		byte c[] = {'b','b'};

		byte cArr[] = s.getBytes();
		System.out.println(cArr);
		System.out.println(c);
		
		for(int i=0;i<c.length;i++){
			System.out.println(c[i]);
		}
		
		System.out.println("------------------------");
		
		for(int i=0;i<cArr.length;i++){
			System.out.println(cArr[i]);
		}
		
	}
}
