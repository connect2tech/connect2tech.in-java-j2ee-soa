package com.c2t.primitives;

public class BinaryHex {
	public static void main(String[] args) {
		char c = 97;
		System.out.println(c);
		System.out.println(Integer.toBinaryString(c));
		System.out.println(Integer.toHexString(c));
		
		int i = Integer.parseInt(Integer.toBinaryString(c), 2);
		System.out.println(i);
	}
}
