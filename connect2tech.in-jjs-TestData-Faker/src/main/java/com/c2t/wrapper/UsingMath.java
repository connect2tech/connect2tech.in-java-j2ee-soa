package com.c2t.wrapper;

import java.lang.Math;

public class UsingMath {
	public static void main(String[] args) {
		int a = 10;
		int b = 20;
		
		int maxVal = Math.max(a, b);
		System.out.println(maxVal);
	}
}
