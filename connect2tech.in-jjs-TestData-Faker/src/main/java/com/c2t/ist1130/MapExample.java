package com.c2t.ist1130;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class MapExample {
	public static void main(String[] args) {
		Map <String, String> m = new HashMap<String, String>();
		
		m.put("key1", "value1");
		m.put("key2", "value2");
		
		System.out.println(m);
		
		String val= m.get("key1");
		System.out.println(val);
		
		
		Set keys = m.keySet();
		
		Iterator <String> iter = keys.iterator();
		
		while(iter.hasNext()){
			String key = iter.next();
			String value = m.get(key);
			System.out.println("key="+key);
			System.out.println("value="+value);
		}
		
	}
}
