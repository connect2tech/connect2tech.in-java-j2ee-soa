package com.c2t.ist830;



class MyCustomException extends RuntimeException{

	String msg;
	
	MyCustomException(String message){
		msg = message;
	}
	
	public String myMessage(){
		return msg;
	}

}



public class MyException {
	public static void main(String[] args) {
		
		int a = 0;
		int b = 20;
		int c = 0;
			
		try{
			 
			if(a == 0){
				throw new MyCustomException("This is MyCustomException");
			}
			c = b/a;
			
			
		}catch(MyCustomException e){
			System.out.println(e.myMessage());
		}
		
		
		
	}
}
