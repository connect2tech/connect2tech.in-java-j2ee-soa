package com.c2t.ist830;

class Parent{
	Parent(){
		System.out.println("I am Parent...");
	}
}

class Child extends Parent{
	Child() {
		super();
		System.out.println("I am child...");
	}
}

public class ConstructorChaining {
	public static void main(String[] args) {
		Child c  =new Child();
	}
}
