package com.c2t.ist830;

import java.util.Scanner;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class Car{
	public void startEngine(){
		System.out.println("I am startEngine...");
	}
}

class Maruti extends Car{
	
}

class BMW extends Car{
	public void startEngine(){
		System.out.println("I am startEngine, with more power...");
	}
}

public class MethodOverriding {
	public static void main(String[] args) {
		
		Car c;
		Scanner scan = new Scanner(System.in);
		System.out.println("Choose your car...");
		String car = scan.next();
		System.out.println("car="+car);
		
		if(car.equals("Maruti")){
			c = new Maruti();
		}else{
			c = new BMW();
		}
		
		c.startEngine();
		
	}
}
