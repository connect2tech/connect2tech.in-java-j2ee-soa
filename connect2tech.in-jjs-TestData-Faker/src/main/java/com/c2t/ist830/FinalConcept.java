package com.c2t.ist830;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class Bike {

	String color;
	final int fuelCapacity = 10;
	
	final public void startEngine(){
		System.out.println("Engine...");
	}
}

class BmwBike extends Bike{
	/*public void startEngine(){
		System.out.println("BMW Engine...");
	}*/
}

public class FinalConcept {
	public static void main(String[] args) {
		Bike b = new Bike();
		b.color = "blue";
		//b.fuelCapacity = 15;
	}
}
