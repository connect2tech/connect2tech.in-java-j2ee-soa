package com.c2t.ist830;


abstract class Shape{
	String color;
	abstract public void area(int radius);
	public void display(){
		System.out.println("i am display...");
	}
}

class Circle extends Shape{
	public void area(int radius){
		System.out.println(Math.PI * radius * radius);
	}
}


public class AbstactClass {
	public static void main(String[] args) {
		Shape s = new Circle();
		s.area(10);
	}
}
