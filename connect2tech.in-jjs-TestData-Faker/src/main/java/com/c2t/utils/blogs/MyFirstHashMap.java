package com.c2t.utils.blogs;

import java.util.*;

public class MyFirstHashMap {
	public static void main(String[] args) {

		Map<String, String> loans = new HashMap<String, String>();
		loans.put("loan1", "Home Loan");
		loans.put("loan2", "Car Loan");

		Set<String> keySet = loans.keySet();

		Iterator<String> keySetIterator = keySet.iterator();

		System.out.println("###### Iterating Map in Java using KeySet Iterator ######");
		while (keySetIterator.hasNext()) {
			String key = keySetIterator.next();
			System.out.println("key: " + key + " value: " + loans.get(key));
		}
		
		System.out.println();

		Set<Map.Entry<String, String>> entrySet = loans.entrySet();

		System.out.println("###### looping HashMap in Java using EntrySet and java5 for loop ######");
		for (Map.Entry <String, String> entry : entrySet) {
			System.out.println("key: " + entry.getKey() + " value: " + entry.getValue());
		}

	}
}
