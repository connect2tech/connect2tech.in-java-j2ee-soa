package com.figmd.session3;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class File3 {
	public void display_public(){
		System.out.println("display3...");
	}
	
	void display_not_public(){
		System.out.println("display3...");
	}
}
