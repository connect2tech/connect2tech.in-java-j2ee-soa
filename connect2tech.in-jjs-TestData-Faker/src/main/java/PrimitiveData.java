
public class PrimitiveData {
	public static void main(String[] args) {
		int a = 10;
		int b = 20;
		int sum = 0;
		
		sum = a++; // a = a+1
		
		System.out.println(sum);
		System.out.println(a);
	}
}
